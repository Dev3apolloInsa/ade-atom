export PATH="$PATH:/opt/atom/bin"
export ATOM_HOME="$HOME/.atom"
export BROWSER='/usr/bin/chromium-browser --no-sandbox'
